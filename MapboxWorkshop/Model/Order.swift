//
//  Order.swift
//  MapboxWorkshop
//
//  Created by Jan Pacek on 06/07/2018.
//  Copyright © 2018 Jan Pacek. All rights reserved.
//

import UIKit
import Mapbox

struct Order: Decodable {

    enum CodingKeys: String, CodingKey {
        case lat
        case long
        case isDelayed = "is_delayed"
        case pizzaCount = "pizza_count"
    }

    let coordinate: CLLocationCoordinate2D
    let isDelayed: Bool
    let pizzaCount: Int

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let lat = try container.decode(Double.self, forKey: .lat)
        let long = try container.decode(Double.self, forKey: .long)
        self.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.isDelayed = try container.decode(Bool.self, forKey: .isDelayed)
        self.pizzaCount = try container.decode(Int.self, forKey: .pizzaCount)
    }

}
