//
//  ViewController.swift
//  MapboxWorkshop
//
//  Created by Jan Pacek on 13/06/2018.
//  Copyright © 2018 Jan Pacek. All rights reserved.
//

import UIKit
import Mapbox

class ViewController: UIViewController {

    /* map view reference */
    var mapView: MGLMapView!

    /*
     * FirestoreService will observe changes
     * in the 'orders' collection in Firestore.
     * Every Order has it's coordinate (latitude, longitude),
     * isDelayed flag and pizzaCount
     * (number of pizzas in the order).
     */
    lazy var ordersService: FirestoreService<Order> = {
        return FirestoreService<Order>(collection: "orders")
    }()

    /*
     * FirestoreService will observe changes
     * in the 'couriers' collection in Firestore.
     * There will be only one Courier.
     * Courier has it's coordinate (latitude, longitude)
     * and a route.
     */
    lazy var couriersService: FirestoreService<Courier> = {
        return FirestoreService<Courier>(collection: "couriers")
    }()

    override func viewDidLoad() {
        /*
         * Make sure that MGLMapboxStyleURL
         * is set in the Info.plist
         * along with MGLMapboxAccessToken
         */
        guard let styleURL = URL.mapboxStyleURL else {
            fatalError("MGLMapboxStyleURL must be set in Info.plist")
        }

        /*
         * Initialize a map view with a style URL
         * and keep a reference to it on self.
         */
        mapView = MGLMapView(frame: CGRect.zero, styleURL: styleURL)

        /* Disable unnecessary movements - rotate and pitch. */
        mapView.allowsRotating = false
        mapView.isPitchEnabled = false

        /*
         * Set delegate to self.
         * See the extension MGLMapViewDelegate below.
         */
        mapView.delegate = self

        /*
         * Add the map view to view
         * and configure autolayout constraints.
         */
        mapView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mapView)
        mapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    }

    /*
     * We'll add the pizzeria icon layer
     * to the map style here.
     *
     * Note: you can add and modify layers of MGLStyle
     * once the delegate didFinishLoading method has been called.
     *
     */
    func addPizzeria(to style: MGLStyle) {
        /*
         * First we need a Source.
         * A map content Source supplies content
         * to be shown on the map.
         *
         * There are different types of Sources,
         * we'll use a MGLShapeSource.
         *
         * MGLShapeSource supplies vector shapes
         * - points, lines, polygons, etc.
         *
         */

        /* check if pizzeria Source is already in the map */
        guard style.source(withIdentifier: Pizzeria.identifier) == nil else {
            /* if yes - do nothing */
            return
        }

        /* create a point Shape with pizzeria coordinate */
        let shape = MGLPointAnnotation()
        shape.coordinate = Pizzeria.coordinate

        /* create a shape Source with unique
         * identifier and the pizzeria Shape
         */
        let source = MGLShapeSource(identifier: Pizzeria.identifier, shape: shape, options: nil)

        /* add the source to the style */
        style.addSource(source)

        /*
       * create a symbol Layer with unique
       * identifier and the pizzeria Source
       */
        let layer = MGLSymbolStyleLayer(identifier: Pizzeria.identifier, source: source)

        /* set pizzeria icon image to style */
        style.setImage(Pizzeria.iconImage, forName: Pizzeria.identifier)

        /* set icon name to the pizzeria Layer */
        layer.iconImageName = NSExpression(forConstantValue: Pizzeria.identifier)

        /* set icon opacity */
        layer.iconOpacity = createOpacityExpression()

        /* insert the Layer to map Style */
        insert(layer: layer, to: style, aboveIdentifier: "country-label")
    }

    /*
     * Move the courier along it's route here.
     */
    func move(courier: Courier) {
        /*
         *
         * We'll use Annotations to display
         * the Courier moving along his route.
         *
         * Annotations sit on top of the map
         * and are displayed as views or images.
         *
         * Annotations are compatible with UIKit and Core Animation.
         *
         * Annotations views and images are created
         * by the map view's delegate
         * method viewFor:annotation/imageFor:annotation
         *
         */

        /* find CourierAnnotation in map's annotations. */
        let courierAnnotation = mapView.annotations?.compactMap { $0 as? CourierAnnotation }.first

        if let annotation = courierAnnotation {
            /* if found - animate courier coordinate change */
            UIView.animate(withDuration: 2) {
                annotation.coordinate = courier.coordinate
            }
        } else {
            /* else - create courier annotation */
            let annotation = CourierAnnotation(coordinate: courier.coordinate)

            /* add it to the map view */
            mapView.addAnnotation(annotation)
        }
    }

    /*
     * Update courier's route layer here.
     *
     * Same as the pizzeria layer except for the type of the Shape and the Layer.
     * We'll use the MGLPolyline to represent courier's route (line)
     * and MGLLineStyleLayer to draw it.
     *
     * Route is represented as an array of coordinates.
     *
     */
    func update(route coordinates: [CLLocationCoordinate2D]?) {
        /* make sure the Style has been loaded */
        guard let style = mapView.style else {
            fatalError("style must be loaded")
        }

        var shape: MGLShape?

        if let coordinates = coordinates {
            /* create a line Shape if route is not nil */
            shape = MGLPolyline(coordinates: coordinates, count: UInt(coordinates.count))
        }

        let identifier = "courier"
        /* if the Source is already in style - update it with the new route Shape */
        if let source = style.source(withIdentifier: identifier) as? MGLShapeSource {
            /*
             * Just settings the new Shape will make the change happen.
             * We can set it to nil and it will disappear.
             */
            source.shape = shape
            return
        }

        guard let routeShape = shape else { return }

        /* create a route Source with the line Shape */
        let source = MGLShapeSource(identifier: identifier, shape: routeShape, options: nil)

        /* add the source to the style */
        style.addSource(source)

        /* create a line Layer with the line Source */
        let layer = MGLLineStyleLayer(identifier: identifier, source: source)

        /* set the color */
        layer.lineColor = NSExpression(forConstantValue: UIColor.black)
        /* set the width */
        layer.lineWidth = NSExpression(forConstantValue: Float(2))
        /* set the opacity */
        layer.lineOpacity = createOpacityExpression()

        /* insert the Layer to the map Style */
        insert(layer: layer, to: style, aboveIdentifier: "road")
    }

    /*
     * Update orders' layer here.
     *
     * Same as before except we'll use
     * Features instead of Shapes.
     * Features are Shapes with additional properties
     * - attributes (simplified).
     *
     * Every Order will be represented as a Point Feature
     * with is_delayed and pizza_count attributes.
     *
     * Together they'll form a Feature Collection.
     *
     */
    func update(orders: [Order]) {
        /* make sure the Style has been loaded */
        guard let style = mapView.style else {
            fatalError("style must be loaded")
        }

        /* iterate over Orders to produce an array of Features */
        let features = orders.map { order -> MGLPointFeature in
            let point = MGLPointFeature()
            /* every Point Feature has a coordinate */
            point.coordinate = order.coordinate
            /* and attributes */
            point.attributes = [
                "is_delayed": order.isDelayed,
                "pizza_count": order.pizzaCount
            ]
            return point
        }
        /* create a Feature Collection for the Source */
        let shape = MGLShapeCollectionFeature(shapes: features)

        let identifier = "orders"
        /* if the Source is already in style - update it with the new Shape */
        if let source = style.source(withIdentifier: identifier) as? MGLShapeSource {
            source.shape = shape
            return
        }

        /* create the orders' Source with the Feature Collection as it's Shape */
        let source = MGLShapeSource(identifier: identifier, shape: shape, options: nil)

        /* add the source to the style */
        style.addSource(source)

        /* create a circle Layer with the shape Source */
        let layer = MGLCircleStyleLayer(identifier: "orders", source: source)

        /* set the stroke color - border color */
        layer.circleStrokeColor = NSExpression(forConstantValue: UIColor.white)
        /* set the stroke width - border width */
        layer.circleStrokeWidth = NSExpression(forConstantValue: Float(2))

        /*
         * Here we'll use advanced Expressions to dynamically
         * style the layer based on the Feature attributes.
         *
         * It's called data-driven styling and it sooooo powerful...
         *
         *
         * Use Conditional Expression to vary the circle color
         * based on the is_delayed attribute.
         *
         * If is_delayed is true - use red color, otherwise green.
         *
         */
        layer.circleColor = NSExpression(
            forConditional: NSPredicate(format: "is_delayed = YES"),
            trueExpression: NSExpression(forConstantValue: UIColor.red),
            falseExpression: NSExpression(forConstantValue: UIColor.green)
        )

        /*
         * We'll use the pizza_count attribute
         * to determine the size (radius) of the circles.
         *
         * Radius steps is a dictionary of pizza_count:radius pairs.
         *
         */
        let radiusSteps: [Int: Float] = [
            1: Float(10),
            3: Float(12),
            5: Float(16),
            9: Float(20)
        ]

        /*
         * Use Stepping Expression to vary the circle color
         * based on the is_delayed attribute.
         *
         * If is_delayed is true - use red color, otherwise green.
         *
         */
        layer.circleRadius = NSExpression(
            forMGLStepping: NSExpression(forKeyPath: "pizza_count"),
            from: NSExpression(forConstantValue: Float(10)),
            stops: NSExpression(forConstantValue: radiusSteps)
        )
        /* set the opacity of the circles */
        layer.circleOpacity = createOpacityExpression()
        /* set the opacity of the circle borders */
        layer.circleStrokeOpacity = createOpacityExpression()

        /* insert the Layer to the map Style */
        insert(layer: layer, to: style, belowIdentifier: "country-label")
    }

    /* create expression that fades layer above zoom level 11 */
    func createOpacityExpression() -> NSExpression {
        let zoomBreakpoint: Float = 11.0
        let opacityStops: [Float: NSExpression] = [
            zoomBreakpoint: NSExpression(forConstantValue: Float(0)),
            zoomBreakpoint + 0.1: NSExpression(forConstantValue: Float(1))
        ]
        return NSExpression(
            forMGLInterpolating: NSExpression.zoomLevelVariable,
            curveType: .linear,
            parameters: nil,
            stops: NSExpression(forConstantValue: opacityStops)
        )
    }

    /*
     * Utility function to insert a new layer
     * either above or below another layer.
     * Or add layer on top of all other layers.
     *
     * Note: you can only insert a layer once.
     * Inserting the same layer more then once
     * will crash the app.
     *
     */
    func insert(layer: MGLStyleLayer, to style: MGLStyle, aboveIdentifier: String? = nil, belowIdentifier: String? = nil) {
        if let aboveIdentifier = aboveIdentifier, let above = style.layer(withIdentifier: aboveIdentifier) {
            style.insertLayer(layer, above: above)
        } else if let belowIdentifier = belowIdentifier, let below = style.layer(withIdentifier: belowIdentifier) {
            style.insertLayer(layer, below: below)
        } else {
            style.addLayer(layer)
        }
    }

    /*
     * Utility function to setup Firestore Services
     * to observe data changes. Call after didFinishLoading.
     *
     */
    func startObservingFirestore() {
        /*
         * On every update of the 'orders' collection,
         * call update(orders:) to add/update the orders layer.
         */
        ordersService.onCollectionUpdate = { [weak self] orders in
            self?.update(orders: orders)
        }
        /* start updating orders */
        ordersService.startObservingCollection()

        /*
         * On every update of the 'orders' collection,
         * call move(courier:) to animate courier
         * along it's route and update(route:)
         * to add/update route layer.
         */
        couriersService.onCollectionUpdate = { [weak self] couriers in
            /* there is only one Courier */
            guard let courier = couriers.first else { return }

            self?.move(courier: courier)
            self?.update(route: courier.route)
        }
        /* start updating couriers */
        couriersService.startObservingCollection()
    }

}

/*
 * In MGLMapViewDelegate you can respond to map view events
 * - camera changes, map movement,
 *   user's location updates, etc.
 */
extension ViewController: MGLMapViewDelegate {

    /*
     * didFinishLoading will be called only once
     * with the MGLStyle map style.
     * We can modify the map style
     * and add layers to it after this method is called.
     */
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        /*
       * Create a MGLMapCamera that overviews
       * Bologna from a 20km altitude above the city.
       */
        let initialCamera = MGLMapCamera(
            lookingAtCenter: Pizzeria.coordinate,
            altitude: 20000,
            pitch: 0,
            heading: 0
        )

        /* set the camera without animation */
        mapView.setCamera(initialCamera, animated: false)

        /* add the pizzeria marker */
        addPizzeria(to: style)

        /* observe changes in Firestore */
        startObservingFirestore()
    }

    /*
     * Map view will call this delegate
     * method to create/reuse Annotation view.
     */
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        guard annotation is CourierAnnotation else {
            return nil
        }
        let reuseIdentifier = "courier"

        /* try to reuse Annotation view */
        if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) {
            return annotationView
        }
        /* create the Annotation view - it's just a UIView subclass */
        let annotationView = MGLAnnotationView(reuseIdentifier: reuseIdentifier)
        let size = CGSize(width: 30, height: 30)
        annotationView.bounds = CGRect(origin: CGPoint.zero, size: size)
        annotationView.backgroundColor = UIColor.black
        annotationView.layer.cornerRadius = size.width / 2
        annotationView.layer.borderColor = UIColor.white.cgColor
        annotationView.layer.borderWidth = 2
        annotationView.layer.masksToBounds = true
        return annotationView
    }

}
