//
//  FirestoreService.swift
//  MapboxWorkshop
//
//  Created by Jan Pacek on 08/07/2018.
//  Copyright © 2018 Jan Pacek. All rights reserved.
//

import FirebaseFirestore
import CodableFirebase

private struct UnknownError: Error {}

class FirestoreService<DocumentType: Decodable>: NSObject {

    var onCollectionUpdate: (([DocumentType]) -> Void)?

    private let db: Firestore
    private let collectionRef: CollectionReference
    private var listener: ListenerRegistration?

    init(collection: String, db: Firestore = Firestore.firestore()) {
        self.db = db
        self.collectionRef = db.collection(collection)
        super.init()
    }

    func startObservingCollection() {
        stopObservingCollection()
        listener = collectionRef.addSnapshotListener { [weak self] snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                self?.handle(error: error ?? UnknownError())
                return
            }

            self?.decode(snapshot: snapshot)
        }
    }

    func stopObservingCollection() {
        listener?.remove()
        listener = nil
    }

    private func decode(snapshot: QuerySnapshot) {
        DispatchQueue(label: collectionRef.collectionID, qos: .background).async {
            do {
                let decoder = FirestoreDecoder()
                let documents = try snapshot.documents.map { try decoder.decode(DocumentType.self, from: $0.data()) }
                DispatchQueue.main.async {
                    self.onCollectionUpdate?(documents)
                }
            } catch {
                self.handle(error: error)
            }
        }
    }

    private func handle(error: Error) {
        print("ERROR: \(error.localizedDescription)")
    }

}
