//
//  Courier.swift
//  MapboxWorkshop
//
//  Created by Jan Pacek on 08/07/2018.
//  Copyright © 2018 Jan Pacek. All rights reserved.
//

import UIKit
import Mapbox

class Courier: NSObject, Decodable, MGLAnnotation {

    enum CodingKeys: String, CodingKey {

        case lat
        case long
        case route
    }

    let route: [CLLocationCoordinate2D]?
    let coordinate: CLLocationCoordinate2D

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let lat = try container.decode(Double.self, forKey: .lat)
        let long = try container.decode(Double.self, forKey: .long)
        self.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)

        let routeCoordinates = (try? container.decode([Double].self, forKey: .route)) ?? []
        let coordinates = routeCoordinates.reduce([[Double]]()) { result, coord -> [[Double]] in
            var result = result
            if let last = result.last, last.count == 1 {
                result[result.count - 1].append(coord)
            } else {
                result.append([coord])
            }
            return result
        }.map { CLLocationCoordinate2D(latitude: $0.last!, longitude: $0.first!) }

        self.route = coordinates.isEmpty ? nil : coordinates
    }

}
