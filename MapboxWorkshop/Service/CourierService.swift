//
//  CourierService.swift
//  MapboxWorkshop
//
//  Created by Jan Pacek on 08/07/2018.
//  Copyright © 2018 Jan Pacek. All rights reserved.
//

import FirebaseFirestore
import CodableFirebase

protocol CourierServiceDelegate: class {

    func didUpdate(courier: Courier)
    func failedWith(error: Error)

}

class CourierService: NSObject {

    weak var delegate: CourierServiceDelegate? {
        didSet {
            delegate == nil ? stopObservingCourier() : startObservingCourier()
        }
    }

    internal let db: Firestore
    internal let collectionRef: CollectionReference
    internal var listener: ListenerRegistration?

    init(collection: String = "cars", db: Firestore = Firestore.firestore()) {
        self.db = db
        self.collectionRef = db.collection(collection)
        super.init()
    }

    func startObservingCourier() {
        stopObservingOrders()
        listener = collectionRef.addSnapshotListener { [weak self] snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                self?.delegate?.failedWith(error: error ?? OrdersServiceError.unknown)
                return
            }
            do {
                let decoder = FirestoreDecoder()
                let orders = try snapshot.documents.map { try decoder.decode(Order.self, from: $0.data()) }
                self?.delegate?.didUpdate(orders: orders)
            } catch {
                self?.delegate?.failedWith(error: error)
            }
        }
    }

    func stopObservingCourier() {
        listener?.remove()
        listener = nil
    }

}
