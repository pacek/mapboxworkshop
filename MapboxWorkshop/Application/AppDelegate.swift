//
//  AppDelegate.swift
//  MapboxWorkshop
//
//  Created by Jan Pacek on 13/06/2018.
//  Copyright © 2018 Jan Pacek. All rights reserved.
//

import UIKit
import Mapbox
import Firebase
import FirebaseFirestore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        /* Firestore setup */
        FirebaseApp.configure()
        let db = Firestore.firestore()
        let settings = db.settings
        db.settings = settings
        return true
    }

}

