//
//  CourierAnnotation.swift
//  MapboxWorkshop
//
//  Created by Jan Pacek on 08/07/2018.
//  Copyright © 2018 Jan Pacek. All rights reserved.
//

import Mapbox

class CourierAnnotation: MGLPointAnnotation {

    init(coordinate: CLLocationCoordinate2D) {
        super.init()
        self.coordinate = coordinate
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
