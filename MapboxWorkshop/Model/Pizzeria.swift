//
//  Pizzeria.swift
//  MapboxWorkshop
//
//  Created by Jan Pacek on 06/07/2018.
//  Copyright © 2018 Jan Pacek. All rights reserved.
//

import Foundation
import Mapbox

struct Pizzeria {

    static let identifier = "pizzeria"
    static let iconImage: UIImage = #imageLiteral(resourceName: "pizzeria")
    static let coordinate = CLLocationCoordinate2D(latitude: 44.488584, longitude: 11.342682)

}
